#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <cstring>
#include <unistd.h>
int start_server();

int main(){
	std::cout<<"Start\n"<<std::endl;
	int server = start_server();
	printf("%d\n", server);
	return 0;
}

int start_server(){
	int sock, client, bytes_read;
	struct sockaddr_in addr;
	char buffer[1024];

	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock < 0){
		return 1;
	}
	addr.sin_family = AF_INET;
	addr.sin_port = htons(8189);
	addr.sin_addr.s_addr = htons(INADDR_ANY); // 127.0.0.1

	if (bind(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0){
		return 2;
	}

	listen(sock,1);
	client = accept(sock, NULL, NULL);
 	if(client == 0)
        {
            return 3;
        }
	while(1){
		bytes_read = recv(client, buffer, 1024, 0);
		if(bytes_read <= 0){
			continue;
		} 
		if (buffer[0] == '/' and buffer[1] == 'q'){
			break;
		}
        std::cout<<"Buffer: "<<buffer<<std::endl;
        memset(buffer, 0, sizeof(buffer));
	}
	close(client);
	return 0;
}